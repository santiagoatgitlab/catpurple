<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function save_entry()
	{
		
		$this->load->model('Page_model','',TRUE);
		$data = $_POST;
		$data['insert'] = $this->Page_model->save_entry($data);
		
		if ($data['insert']['success']){

			$data['insert']['content'] = $this->load->view('entry_text',$data,true);

			if (isset($data['insert']['exception_code']) && $data['insert']['exception_code'] == POSITION_TAKEN_WARNING) {
				$new_location = $data['insert']['new_entry_location'];
				$this->insert_cookie_hash($data['insert']['insert_id'],$new_location['animal_name'],$new_location['color_name'],$new_location['position']);
			}
	
			$this->insert_cookie_hash($data['insert']['insert_id'],$data['animal_name'],$data['color_name'],$data['position']);
		}
		echo json_encode($data['insert']);
		
	}
	
	public function load_entries(){
		
		$this->load->model('Page_model','',TRUE);		
		
		$response['success'] = false;
		$data = $_POST;
		$data['read_only'] = (boolean)$data['read_only'];
		$data['to'] = $data['from'] + MAX_ENTRIES_PER_LOAD - 1;
		$data['entries'] 	= $this->Page_model->get_entries($data);
		$response['success'] = true;
		$response['content'] = $this->load->view('entries',$data,true);
		echo json_encode($response);
	}

	function insert_cookie_hash($insert_id,$animal_name,$color_name,$position){
	
		$cookie_hash_generated = $this->Page_model->insertQuickEditLine($insert_id);
		if ($cookie_hash_generated['success']){
			setcookie("_".$animal_name."_".$color_name."_".$position,$cookie_hash_generated['cookie_hash'], time()+(60*QUICK_EDIT_TIME), '/' );
			$_COOKIE['_'.$animal_name.'_'.$color_name.'_'.$position] = $cookie_hash_generated['cookie_hash'];
		}
	}
	
}
