<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($animal_name,$color_name,$position)
	{
		
		$this->load->model('General_model','',TRUE);		
		$this->load->model('Page_model','',TRUE);		
		$this->load->model('Navigation_model','',TRUE);		
		$this->load->model('Entry_model','',TRUE);
		
		$data = array("animal_name"=>$animal_name,"color_name"=>$color_name);
		$data['script_path'] 	= 'entry.js';
		$data['animal_id'] 		= $this->Page_model->get_animal_id($animal_name);
		$data['color_id'] 		= $this->Page_model->get_color_id($color_name);
		$data['position'] 		= $position;
		$data['edit_button']	= false;
		
		if (isset($_POST['content'])){
			$data['content'] = $_POST['content'];

			if (isset($_POST['edit'])){
				$data['update'] = $this->Page_model->update_entry($data);
				unset($_POST['edit']);
			}
			else{
				$data['insert'] = $this->Page_model->save_entry($data);
				if ($data['insert']['success']){ 
					if (isset($data['insert']['exception_code']) && $data['insert']['exception_code'] == POSITION_TAKEN_WARNING) {
						
						$new_location = $data['insert']['new_entry_location'];
						$redirect = $new_location['animal_name']."/".$new_location['color_name']."/".$new_location['position'];
						
						$this->insert_cookie_hash($data['insert']['insert_id'],$new_location['animal_name'],$new_location['color_name'],$new_location['position']);
						$this->session->set_flashdata("show_location_taken_message",true);
						redirect(DOMAIN_ROOT.$redirect);

					}
					$this->insert_cookie_hash($data['insert']['insert_id'],$animal_name,$color_name,$position);
				}
			}	
		}
		
		if (isset($_COOKIE['_'.$animal_name.'_'.$color_name.'_'.$position])){
			$data['edit_button'] = $this->Entry_model->is_editable($_COOKIE['_'.$animal_name.'_'.$color_name.'_'.$position]); 
		}

		$response = $this->Page_model->get_entry($animal_name,$color_name,$position);
		if ($response['status'] == 0){
			$entry = $response['entry'];
		}
		else if (  $response['error_type'] == NON_EXISTANT_ANIMAL || $response['error_type'] == NON_EXISTANT_COLOR ){
			redirect(DOMAIN_ROOT);
		}
		
		if ($position == 0 || $position >= 1000000 || ($position > 1000 && $position % 1000 != PUBLIC_HIDDEN_ENTRIES)){
			redirect(DOMAIN_ROOT);
		}
	
		if ($this->session->flashdata("show_location_taken_message")){
			$data["show_location_taken_message"] = true;
		}
		if ($position > 999){
			$data['thousand_numbers'][] = floor(($position % 10000) / 1000);
			if ($position > 9999){
				$data['thousand_numbers'][] = floor(($position % 100000) / 10000);
				if ($position > 99999){
					$data['thousand_numbers'][] = floor($position / 100000);
				}
			}
			rsort($data['thousand_numbers']);
		}		
		if ($position > 99){
			$data['hundred_number'] = floor(($position % 1000) / 100);
		}
		if ($position > 9){
			$data['ten_number'] = floor(($position % 100) / 10);
		}
		$data['unit_number'] = $position % 10;

		$data['edit'] = false;			
		if (isset($_POST['edit'])){
			$data['edit'] = true;
		}
		
	
		$previous_entry	= $this->Navigation_model->get_previous_entry($position,$data['color_id'],$data['animal_id']);
	 	$next_entry		= $this->Navigation_model->get_next_entry($position,$data['color_id'],$data['animal_id']);
		
		$data['previous_link'] = "/" . $previous_entry['animal_name'] . "/" .  $previous_entry['color_name'] . "/" . $previous_entry['position'];
		$data['next_link'] = "/" . $next_entry['animal_name'] . "/" .  $next_entry['color_name'] . "/" . $next_entry['position'];
		
		if (isset($entry['insert_time'])){
			$date = new DateTime(  explode(" ",$entry['insert_time'])[0] );
			$data['content'] 	= $entry['content'];
			$data['date'] 		= $date->format('F d, Y');
		}
		
		$this->load->view('header',$data);
		$this->load->view('entry',$data);
		$this->load->view('footer',$data);

	}
	
	function insert_cookie_hash($insert_id,$animal_name,$color_name,$position){
	
		$cookie_hash_generated = $this->Page_model->insertQuickEditLine($insert_id);
		if ($cookie_hash_generated['success']){
			setcookie("_".$animal_name."_".$color_name."_".$position,$cookie_hash_generated['cookie_hash'], time()+(60*QUICK_EDIT_TIME), '/' );
			$_COOKIE['_'.$animal_name.'_'.$color_name.'_'.$position] = $cookie_hash_generated['cookie_hash'];
		}
	
	}

}
