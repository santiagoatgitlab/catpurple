<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function last_entries($all=false)
	{
		
		$this->load->model('Search_model','',TRUE);
		$data['additional_css'] = array("search.css");
		$data['script_path'] 	= "last100.js";
		$data['last_entries'] 	= $this->Search_model->get_last_entries(100,$all);

		$data['bc']				= "/ last 100";
		$data['previous_link']	= "/";

		$this->load->view('header',$data);
		$this->load->view('last_entries',$data);
		$this->load->view('footer',$data);
		
	}
	
		
		
		
}
