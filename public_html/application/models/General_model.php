<?php 
class General_model extends CI_Model {

	public function get_animals()
	{
		$query = $this->db->query('SELECT id,name,picture FROM animals LIMIT 15');
		return $query->result_array();
	}

	public function get_colors()
	{
		$query = $this->db->query('SELECT id,name FROM colors LIMIT 15');
		return $query->result_array();
	}

		
}
