<?php 
class Search_model extends CI_Model {
	
	
	public function get_last_entries($limit,$all)
	{
		$query = "SELECT a.name as animal_name, c.name as color_name, e.position, e.insert_time, e.content FROM entries e
				JOIN animals a ON a.id = e.animal_id 
				JOIN colors c ON c.id = e.color_id";
		if (!$all){ $query .= " WHERE e.position < 1001 "; }
		$query .= " ORDER BY insert_time DESC LIMIT " . $limit;
				
		$result = $this->db->query($query)->result_array();
		
		return $result;
				
	}
}
