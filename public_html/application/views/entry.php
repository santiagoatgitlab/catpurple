<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="entry">
	<div id="header" class="entry <?php echo $animal_name; ?> <?php echo $color_name; ?>">
		<div class="animal-figure"><img src="/assets/img/sprite-texts-bg.png" onload="show_container()"></div>
		<div class="space"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="animal-name"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="bracket"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="color-name"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="bracket"><img src="/assets/img/sprite-texts-bg.png"></div>
		<?php if (isset($thousand_numbers)) foreach ($thousand_numbers as $thousand_number) { ?>
			<div><img src="/assets/img/numbers/<?php echo $thousand_number; ?>.png"></div>
		<?php } ?>
		<?php if (isset($hundred_number)) { ?><div><img src="/assets/img/numbers/<?php echo $hundred_number; ?>.png"></div><?php } ?>
		<?php if (isset($ten_number)) { ?><div><img src="/assets/img/numbers/<?php echo $ten_number; ?>.png"></div><?php } ?>
		<div><img src="/assets/img/numbers/<?php echo $unit_number; ?>.png"></div>
	</div>
	<div id="content" class="w3-center">
		<?php if (empty($content) || $edit){ ?>
			<div class="inputs">
				<form method="post" target="">
				<?php if (empty($content)){ ?>
				<textarea spellcheck="false" name="content" maxlength="34100" class="empty">No body ever wrote here, be the first and only one</textarea>
				<?php } else { ?>
				<textarea spellcheck="false" name="content" maxlength="34100"><?php echo $content; ?></textarea>
				<?php } if ($edit) { ?>
					<input type="hidden" name="edit">
				<?php } ?>
				<input type="submit" class="save" value="save" disabled>
				</form>
			</div>			
		<?php } else { ?>
			<?php if ($edit_button) { ?>	
			<div class="edit w3-display-container">
				<form method="post" action="">
					<input type="submit" value="edit" class="w3-display-left" name="edit">
				</form>
			</div>
			<?php } else { ?>
			<div class="date w3-display-container"><label class="w3-display-right"><?php echo $date; ?></label></div>
			<?php } ?>
			<div class="individual-entry">
				<label><?php echo $content; ?></label>
			</div>
		<?php } ?>
		<input type="hidden" id="animal-id" value="<?php echo $animal_id; ?>">
		<input type="hidden" id="color-id" value="<?php echo $color_id; ?>">
		<input type="hidden" id="position" value="<?php echo $position; ?>">
		<?php if (isset($show_location_taken_message)) { ?>
		<div class="message-modal ">
			<div class="box w3-display-middle w3-display-container">
				<div class="box-content w3-display-middle">
					<h1>ADVERTENCIA</h1>
					<label>
						The position where you tried to write was taken while you were writting.<br>your post was moved to the closest next free space which is:
						<h3>/<?php echo $animal_name.'/'.$color_name.'/'.$position; ?></h3>
					</label><br><br><br>
					<input type="button" class="ok" value="ok">
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<img class="image-loader" src="/assets/img/sprite-animals.png">
<img class="image-loader" src="/assets/img/sprite-colors.png">
