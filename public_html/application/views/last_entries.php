<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="last_entries">
	<div class="lines">
		<?php foreach ($last_entries as $entry) { ?>
			<div class="line">
				<div class="side-div img-bg <?php echo $entry['animal_name']." ".$entry['color_name']; ?>">
					<a href="/<?php echo $entry['animal_name']."/".$entry['color_name']; ?>"><img src="/assets/img/sprite-texts-bg.png"></a>
				</div>
				<div class="content"><label><?php echo $entry['content']; ?></label></div>
				<div class="side-div position w3-display-container <?php echo $entry['animal_name']." ".$entry['color_name']; ?>">
					<label class="w3-display-middle"><?php echo $entry['position'];?></label>
				</div>
				<div class="theHidden">
					<input type="hidden" class="animal-name" value="<?php echo $entry['animal_name']; ?>">
					<input type="hidden" class="color-name" value="<?php echo $entry['color_name']; ?>">
					<input type="hidden" class="position-val" value="<?php echo $entry['position']; ?>">
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="bc w3-display-container">
		<a class="bc-link w3-display-middle" href="/">catpurple.net</a>
	</div>
</div>
