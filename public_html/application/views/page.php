<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="page <?php echo $color_name; ?>">
	<div id="header" class="page <?php echo $animal_name; ?> <?php echo $color_name; ?>">
		<div class="animal-figure"><img src="/assets/img/sprite-texts-bg.png" onload="show_container()"></div>
		<div class="space"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="animal-name"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="bracket"><img src="/assets/img/sprite-texts-bg.png"></div>
		<div class="color-name"><img src="/assets/img/sprite-texts-bg.png"></div>
	</div>
	<div id="content" class="w3-center">
		<div class="entries">
			<?php for ($i=1; $i <= MAX_ENTRIES_PER_LOAD; $i++) { ?>
				<?php if (isset($entries[$i])){ ?>
				<div class="entry <?php if (!isset($entries[$i])) echo "closed"; else echo "db"; echo " ".$color_name . " " . $i; ?>">
					<div class="number test w3-display-container"><span class="w3-display-middle"><?php echo $i; ?></span></div>
					<div class="content">
						<div class="text w3-display-container">
							<label><?php if ($entries[$i] != "") echo $entries[$i]; else echo "&nbsp;" ?></label>
						</div>
					</div>
					<input type="hidden" class="position" value="<?php echo $i; ?>">
				</div>
				<?php } else if (!$read_only){ ?>	
				<div class="entry <?php if (!isset($entries[$i])) echo "closed"; else echo "db"; echo " ".$color_name . " " . $i; ?>">
					<div class="number w3-display-container"><span class="w3-display-middle"><?php echo $i; ?></span></div>
					<div class="content">
						<div class="invitation"><span>Click to add some text</span></div>
						<div class="saving"><span>Saving...</span></div>
						<div class="inputs">
							<textarea spellcheck="false" maxlength="34100"></textarea>
							<input type="button" class="save" value="save">
							<input type="button" class="discard" value="discard">
						</div>			
					</div>
					<input type="hidden" class="position" value="<?php echo $i; ?>">
				</div>
				<?php } ?>
			<?php } ?>
		</div>
		<?php for($i; $i <= ENTRIES_PER_PAGE; $i += MAX_ENTRIES_PER_LOAD){ ?>
		<div class="collapsed-entries-line <?php echo $color_name; ?>">
			<div class="number w3-display-container"><span class="w3-display-middleleft"><?php echo $i."..."; ?></span></div>
			<div class="content">
				<div class="invitation"><span>Click to load entries <?php echo $i . " to " . ($i + MAX_ENTRIES_PER_LOAD - 1); ?></span></div>
			</div>
			<input type="hidden" class="from" value="<?php echo $i; ?>">
		</div>
		<?php } ?>
	</div>
	<input type="hidden" id="animal-id" value="<?php echo $animal_id; ?>">
	<input type="hidden" id="color-id" value="<?php echo $color_id; ?>">
	<input type="hidden" id="animal-name" value="<?php echo $animal_name; ?>">
	<input type="hidden" id="color-name" value="<?php echo $color_name; ?>">
	<input type="hidden" id="read-only" value="<?php echo $read_only; ?>">
	<div class="message-modal ">
		<div class="box w3-display-middle w3-display-container">
			<div class="box-content w3-display-middle">
				<h1>ADVERTENCIA</h1>
				<label>
					the position where you tried to write has been taken while you were writting<br>your post was moved to the closest next free space which is: <h3></h3>
				</label><br><br><br>
				<input type="button" class="ok" value="ok">
			</div>
		</div>
	</div>
</div>
<div id="loading" class="w3-display-container">
	<div class="w3-display-middle">
		<label>catpurple.net</label>
	</div>
</div>
<img class="image-loader" src="/assets/img/sprite-animals.png">
<img class="image-loader" src="/assets/img/sprite-colors.png">

