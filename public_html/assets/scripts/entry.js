$(document).ready(function(){
	init_textarea();
	init_modal();
});
function init_textarea(){
	$('.inputs textarea').keyup(function(){
		adapt_textarea_size($(this));
	});
	$('.inputs textarea').click(function(){
		init_writing($(this));
	});
}
function adapt_textarea_size($textareaElement){
    if($textareaElement.get(0).offsetHeight < $textareaElement.get(0).scrollHeight){
        $textareaElement.css('height',($textareaElement.get(0).scrollHeight*1.05)+'px');
    }	
}
function init_writing($textareaElement){
	if ($textareaElement.hasClass("empty")){
		$textareaElement.html("");
		$textareaElement.removeClass('empty');
	}
	$('.save').removeAttr('disabled').css('color','#777').css('cursor','pointer');
	$textareaElement.unbind("click");
}
function init_modal(){
	$('.message-modal input.ok').click(function(){
		$('.message-modal').hide();
	});
}
function show_container(){
	$('#container').show();
	$('#loading').remove();
}
