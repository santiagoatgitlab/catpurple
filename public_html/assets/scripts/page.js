$(document).ready(function(){
	init_modal();
	$entries_batch = $('.entries');
	init_closed_entries($entries_batch);
	$('.collapsed-entries-line:first').each(function(){
		load_entries($(this));
	})
});

$( window ).resize(function() {
	//checkAllRMButtons();
});
function init_closed_entries($entries_batch){
	$entries_batch.find('.inputs-container').hide();
	$entries_batch.find('.entry .inputs').hide();
	$entries_batch.find('.entry .inputs input.save').click(function(){
		save_entry($(this).parent().parent().parent());
	});
	$entries_batch.find('.entry .inputs input.discard').click(function(){
		close_entry($(this).parent().parent().parent());
	});
	$entries_batch.find('.entry.closed').click(function(){
		open_entry($(this));
	});
	$entries_batch.find('.entry.db').click(function(){
		//window.open('/'+$('#animal-name').val()+'/'+$('#color-name').val()+'/'+$(this).find('.position').val());
		window.location.href='/'+$('#animal-name').val()+'/'+$('#color-name').val()+'/'+$(this).find('.position').val();
	});
	//checkBatchRMButtons($entries_batch);
}

function open_entry($entry){
	$entry.removeClass("closed");
	$entry.addClass("open");
	$entry.find(".invitation").slideUp(100);
	$entry.find(".inputs").slideDown(100);
	$entry.find("textarea").focus();
	$entry.unbind("click");
	$('html, body').animate({
        scrollTop: $entry.offset().top
    }, 600);	
}

function close_entry($entry){
	$entry.removeClass('open');
	$entry.addClass('closed');
	$entry.find(".inputs").slideUp(100);
	$entry.find(".saving").hide();
	$entry.find(".invitation").slideDown(100);
	setTimeout(function(){
		$entry.click(function(){
			open_entry($entry);
		});
	},100);
}

function save_entry($entry){
	$animal_id 		= $('#animal-id').val();
	$color_id 		= $('#color-id').val();
	$animal_name	= $('#animal-name').val();
	$color_name 	= $('#color-name').val();
	$position		= $entry.find(".position").val();
	$content 		= $entry.find("textarea").val();
	$entry.removeClass('open');
	$entry.addClass('db');
	$entry.find('.invitation').hide();
	$entry.find('.saving').show();
	$entry.find('.inputs').hide();
	// $entry.find('.inputs').html('<span style="color:white">Saving...</span>');
	$.ajax({
		url:"/ajax/save_entry",				
		data: {animal_id:$animal_id,color_id:$color_id,animal_name:$animal_name,color_name:$color_name,position:$position,content:$content},
		method: "POST",
		dataType: "json",
		success: function($response){
			if ($response.success){
				$entry_exists = true;
				$wrapping_a = '<a href="/'+$animal_name+'/'+$color_name+'/'+$position+'"></a>';
				if ($response.exception_code == 1){ // POSITION_TAKEN_WARNING
					close_entry($entry);
					if ($response.new_entry_location.animal_name == $('#animal-name').val() && 
						$response.new_entry_location.color_name == $('#color-name').val()){
						if ($('.entry.'+$response.new_entry_location.position).length > 0){
							$entry = $('.entry.'+$response.new_entry_location.position);
							$entry_exists = true;		
							$wrapping_a = '<a href="/'+$response.new_entry_location.animal_name+'/'+
								$response.new_entry_location.color_name+'/'+$response.new_entry_location.position+'"></a>';
							$position = $response.new_entry_location.position;
						}
						$('.message-modal label h3').html($response.new_entry_location.animal_name+'/'+
						$response.new_entry_location.color_name+'/'+$response.new_entry_location.position);
						$('.message-modal').show();
					}
				}
				if ($entry_exists){
					$entry.find(".content").html($response.content);
					$entry.click(function(){
						window.location.href='/'+$('#animal-name').val()+'/'+$('#color-name').val()+'/'+$position;
					});
					//$entry.wrap($wrapping_a);
				}
			}
			else{
				close_entry($entry);
			}
		}
	});
}

function checkAllRMButtons(){
	$('.entries .entry.db').each(function(){
		checkRMButton($(this));
	});
}

function checkBatchRMButtons($entries_batch){
	$entries_batch.find('.entry.db').each(function(){
		checkRMButton($(this));
	});
}

function checkRMButton($entry){
	if($entry.find('.text').get(0).offsetHeight < $entry.find('.text').get(0).scrollHeight){
		$entry.find('.more').show();
	}
	else{
		$entry.find('.more').hide();
	}
}

function load_entries($line){
	$animal_id 		= $('#animal-id').val();
	$color_id 		= $('#color-id').val();
	$animal_name	= $('#animal-name').val();
	$color_name 	= $('#color-name').val();
	$from = $line.find('.from').val();
	$read_only		= $('#read-only').val();
	$line.find('.invitation span').html('cargando');
	$.ajax({
		url:"/ajax/load_entries",
		data: {animal_id:$animal_id,color_id:$color_id,animal_name:$animal_name,color_name:$color_name,from:$from,read_only:$read_only},
		method: "POST",
		dataType: "json",
		success: function($response){
			if ($response.success){
				$line.before($response.content);
				$entries_batch = $line.prev();
				$line.remove();
				init_closed_entries($entries_batch);
				$('.collapsed-entries-line:first').each(function(){
					load_entries($(this));
				})
			}
			else{
				
			}
		}
	});	
}

function init_modal(){
	$('.message-modal input.ok').click(function(){
		$('.message-modal').hide();
	});
}

function show_container(){
	$('#container').show();
	$('#loading').remove();
}
