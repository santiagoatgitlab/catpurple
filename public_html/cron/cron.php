<?php
#$db_link = new mysqli("localhost","bytxtdxg_dbrr56s","tTlBrd6dQql14","bytxtdxg_catpurple");
$db_link 	= new mysqli("localhost","root","","catpurple");
$id_query 	= "SELECT aux FROM log l JOIN log_types lt ON l.type_id = lt.id WHERE lt.name = 'backup' ORDER BY l.id DESC LIMIT 1";
$query_result 	= $db_link->query($id_query);
$last_id = 0;
if ($query_result != false){
	$last_id = $query_result->fetch_assoc()['aux'];
	if (empty($last_id)){
		$last_id = 0;
	}
}
else{
	query_error("error al buscar id de la última entrada respaldada",$id_query,$db_link->error);
}
$select_query 	= "SELECT * FROM entries WHERE id > ".$last_id;
$query_result	= $db_link->query($select_query);
$entries 	= array();
if ($query_result != false){
	while ($row = $query_result->fetch_array(MYSQLI_ASSOC)){
		$entries[] = $row;
	}
}
else{
	query_error("error al intentar seleccionar entradas no respaldadas",$select_query,$db_link->error);
}	
if (!empty($entries)){
	$file_name	= "backups/entries_" . date("Ymd_H:i:s") . "_".count($entries).".bkp";
        $backup_query = "INSERT INTO entries (";
        $first_key = true;
        foreach ($entries[0] as $key => $value){
                        if (!$first_key){
                                $backup_query .= ',';
                        }
                        else{
                                $first_key = false;
                        }
                        $backup_query .= $key;
        }
        $backup_query .= ") VALUES";
        $last_id = 0;	
        for ($i=0; $i < count($entries); $i++){
                $backup_query .= "(";
                $first_value = true;
                foreach ($entries[$i] as $field_value){
                        if (!$first_value){
                                $backup_query .= ",";
                        }
                        else{
                                $first_value = false;
                        }
                        $backup_query .= "'" . $db_link->escape_string($field_value) . "'";		
                }
                $last_id = $entries[$i]['id'];
                $backup_query .= ")";
                if ($i < count($entries)-1){
                        $backup_query .= ",";
                }
                else{
                        $backup_query .= ";".PHP_EOL;
                }
        }
	if (file_put_contents($file_name,$backup_query)){
        	$log_query = "INSERT INTO log (type_id,message,aux) VALUES (2,'se hizo back up de ".count($entries)." entradas.','".$last_id."')";
	}
	else{
		trigger_error("se produjo un error al intentar escribir el archivo",E_USER_ERROR);
	}
}
else{
        $log_query = "INSERT INTO log (type_id,message,aux) VALUES (2,'".utf8_decode("no hay nuevas entradas para hacer back up")."','".$last_id."')";
	echo "no new entries to back up" . PHP_EOL;
}
$query_result = $db_link->query($log_query);
if (!$query_result != false){
	query_error("error al ejecutar la query que loguea lo hecho",$log_query,$db_link->error);
}
function query_error($message,$query,$error){
	$error_message = PHP_EOL . "-------------------------------------------------------------------------------------------";
	$error_message .= PHP_EOL . date('Y/m/d H:i:s') . PHP_EOL;
	$error_message .= $message . PHP_EOL;
	$error_message .= "consulta:" . PHP_EOL; 
	$error_message .= $query . PHP_EOL; 
	$error_message .= "error:" . PHP_EOL; 
	$error_message .= $error;
	$error_message .= PHP_EOL . "-------------------------------------------------------------------------------------------" . PHP_EOL;
	trigger_error($error_message,E_USER_ERROR);
}
?>
